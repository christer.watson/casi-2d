import logging
import sys

import pandas as pd

from vailtools import networks
import train


def arg_parser():
    parser = train.arg_parser()
    parser.add_argument(
        '-s',
        '--samples',
        default=30,
        type=int,
        help='Number of samples used to estimate the training variance.',
    )
    return parser


def training_variance(
        mode='classify_co',
        model_factory=networks.res_u_net,
        samples=30,
        verbose=True,
        **kwargs,
):
    errors = []
    for i in range(samples):
        start = pd.Timestamp.now()
        error = train.train_nn(
            mode=mode,
            model_factory=model_factory,
            verbose=False,
            **kwargs
        )
        errors.append(error)
        logging.info(f'Iteration {i+1}: {pd.Timestamp.now() - start}')
    error_summary = pd.Series(errors).describe()
    logging.info(f'Training Error Distribution Summary:\n{error_summary}')
    logging.info(f'Raw training errors:\n{errors}')
    return errors


if __name__ == '__main__':
    args = vars(arg_parser().parse_args())

    logging.basicConfig(
        filename=f"../models/{args['model_factory'].__name__}/{pd.Timestamp.now().date()}_{args['mode']}.log",
        level=logging.INFO
    )
    if args['verbose']:
        logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    logging.info('Command line call: {}'.format(' '.join(sys.argv)))

    training_variance(**args)
