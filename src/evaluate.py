import logging
import sys
from pathlib import Path

import numpy as np
import pandas as pd
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from scipy import stats
from vailtools import losses, networks
from vailtools.evaluation import augmented_predictions
from vailtools.data.splits import load_splits, split_data

import data
import figures
import train
from confusion import binary_analysis, select_threshold


def arg_parser():
    parser = train.arg_parser()
    parser.description = 'Evaluates a trained neural network on the appropriate test set.'
    parser.add_argument(
        '-sf',
        '--split_file',
        default='splits',
        type=str,
        help='File name, without extension, containing the data splits associated with the model under evaluation.',
    )
    return parser


def evaluate(
        batch_size=8,
        mode='classify_co',
        model_factory=networks.res_u_net,
        noise_std=0.,
        split_file='splits',
        verbose=False,
        **kwargs,
):
    out_path = Path(f'../models/{model_factory.__name__}/figs/{mode}')
    out_path.mkdir(parents=True, exist_ok=True)

    loss_file = sorted(
        Path(f'../models/{model_factory.__name__}').glob(f'*_{mode}_errors.csv'),
        reverse=True,
    )[0]
    y_label = 'Loss' if 'loss' not in kwargs else kwargs['loss'].__name__,
    figures.error_curves(
        loss_file,
        y_label=y_label,
        out_path=out_path / f'{loss_file.stem.split("_")[0]}_errors.pdf',
    )

    # Prepare data and training set
    splits = load_splits(f'../models/{model_factory.__name__}/{mode}_{split_file}.npz')
    x, y = getattr(data, f'load_{mode.split("_")[-1]}_data')()
    _, val, test = split_data(
        x,
        y,
        splits=splits,
        process_func=getattr(data, f'process_{mode}_data')
    )
    x, y = getattr(data, f'process_{mode}_data')(x, y)

    # Load best checkpoint
    model = load_model(
        f'../models/{model_factory.__name__}/{mode}_model.h5',
        custom_objects={'iou_loss': losses.iou_loss})
    test_gen = ImageDataGenerator()

    if 'classify' in mode:
        # Select a threshold value using the validation set
        val_pred = model.predict_generator(
            test_gen.flow(val.x, batch_size=batch_size, shuffle=False),
            steps=int(np.ceil(len(val.x) / batch_size)),
            verbose=verbose,
        )
        threshold = select_threshold(val.y, val_pred)
        logging.info(f'Selected threshold value: {threshold}')

        # Investigate performance on test set
        test_pred = model.predict_generator(
            test_gen.flow(test.x, batch_size=batch_size, shuffle=False),
            steps=int(np.ceil(len(test.x) / batch_size)),
            verbose=verbose,
        )

        # Calculate and log binary performance statistics
        stats = binary_analysis(test.y, test_pred, threshold=threshold)
        logging.info(f'Binary Statistics:\n{stats.to_string()}\n')

        # Create prediction figures for qualitative performance evaluation
        figures.diff_overlays(
            *test,
            test_pred,
            threshold=threshold,
            output_path=f'../models/{model_factory.__name__}/figs/{mode}',
            norm='symlog' if mode == 'classify_density' else None,
        )
        fpr, tpr, _, roc_auc = figures.roc_curve(
            test.y,
            test_pred,
            output_path=f'../models/{model_factory.__name__}/figs/{mode}'
        )
        logging.info(f'Test ROC AUC: {roc_auc:0.4f}')

        fpr_points = np.asarray([10**-i for i in range(10, 0, -1)])
        logging.info(
            f'Selected FPR thresholds:\n{fpr_points}\n'
            f'Corresponding TPR Values:\n{np.interp(fpr_points, fpr, tpr)}'
        )

        # Output a full-cube prediction for future analysis
        full_pred = model.predict_generator(
            test_gen.flow(x, batch_size=batch_size, shuffle=False),
            steps=int(np.ceil(len(x) / batch_size)),
            verbose=verbose,
        )
        data.prediction_to_fits(
            full_pred,
            out_path=f'../models/{model_factory.__name__}/{mode}_prediction.fits'
        )
        data.triplet_to_fits(
            x,
            y,
            full_pred,
            out_path=f'../models/{model_factory.__name__}/{mode}_triplet.fits'
        )

        error = model.evaluate_generator(
            test_gen.flow(*test, batch_size=batch_size, shuffle=False),
            steps=int(np.ceil(len(test.x) / batch_size)),
        )

        logging.info(
            f'Valid/Test Samples: {len(test.x)}, '
            f'Single-shot Test Error: {error:0.6f}'
        )
        if noise_std:
            aug_pred, aug_std = augmented_predictions(
                model,
                test.x,
                batch_size=batch_size,
                noise_std=noise_std,
            )
            logging.info(
                f'30-Shot Test Error: {np.mean(losses.numpy_losses.iou_loss(test.y, aug_pred)):.6f}, '
                f'Mean Std. Dev.: {np.mean(aug_std):.6f}'
            )

    if 'regress' in mode:
        # Investigate performance on test set
        test_pred = model.predict_generator(
            test_gen.flow(test.x, batch_size=batch_size, shuffle=False),
            steps=int(np.ceil(len(test.x) / batch_size)),
            verbose=verbose,
        )
        residuals = test_pred - test.y

        res_score_comps = residual_score(residuals, components=True)
        logging.info(f'Residual Score: {(-np.abs(res_score_comps)).sum()}')
        logging.info(f'Residual Score Components: {res_score_comps}')

        figures.residual_hist(
            residuals,
            xlabel='Residual',
            ylabel='$P(x)$',
            output_path=f'../models/{model_factory.__name__}/figs/{mode}',
        )

        logging.info(f'\n{pd.Series(residuals.flatten(), name="Residuals").describe()}')

        figures.residual_scaling(
            test.x,
            residuals,
            xlabel='Residual',
            ylabel='Input Value',
            output_path=f'../models/{model_factory.__name__}/figs/{mode}',
        )
        figures.residual_scaling_hist(
            test.x,
            residuals,
            xlabel='Residual',
            ylabel='Input Value',
            output_path=f'../models/{model_factory.__name__}/figs/{mode}',
        )

        figures.residual_overlays(
            test.x,
            residuals,
            x_norm='symlog',
            output_path=f'../models/{model_factory.__name__}/figs/{mode}/unscaled',
        )


def residual_score(residuals, components=False):
    """
    Penalizes the given residuals based on deviations from:
        - zero mean
        - zero standard deviation
        - zero skew

    Args:
        residuals: numpy.ndarray, differences between predicted and target values.
        components: bool, True -> return components, False -> return sum

    Returns: float
        The computed residual score.
    """
    components_ = np.asarray([
        np.mean(residuals),
        np.std(residuals),
        stats.skew(residuals, axis=None),
    ])
    if components:
        return components_
    else:
        return -np.abs(components_).sum()


if __name__ == "__main__":
    args = vars(arg_parser().parse_args())

    logging.basicConfig(
        filename=f"../models/{args['model_factory'].__name__}/{pd.Timestamp.now().date()}_{args['mode']}.log",
        level=logging.INFO
    )
    if args['verbose']:
        logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))
    logging.info('Command line call: {}'.format(' '.join(sys.argv)))

    evaluate(**args)
