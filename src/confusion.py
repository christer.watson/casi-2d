"""
Provides methods to calculate confusion matrix statistics and related metrics,
allowing for detailed measurement of binary classifier performance.

See [wikipedia](https://en.wikipedia.org/wiki/Confusion_matrix) for details on
these statistics.
"""


from multiprocessing import Pool

import numpy as np
import pandas as pd
from scipy.optimize import minimize_scalar


def binary_analysis(
        y_true,
        y_pred,
        threshold=None,
        normalize=True,
):
    """
    Calculates several confusion matrix statistics and logs relevant
    information for later use.

    Input data are binarized using the given threshold, or using their mean value
    if a threshold is not provided.

    Generates a comparison images that allow for a qualitative evaluation of the
    performance of a binary classifier.

    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.
        threshold: (int or float) Used to binarize the input data.
        normalize: (bool) Toggles normalization of the raw confusion matrix
            statistics by the image size.

    Returns: (pandas.DataFrame)
        Binary performance statistics.
    """
    bin_data = [binarize(np.squeeze(x), threshold=threshold)
                for x in (y_true, y_pred)]
    results = dict()
    paired_ims = list(zip(*bin_data))

    pool = Pool()

    results['tp'] = pool.starmap(true_positives, paired_ims)
    results['tn'] = pool.starmap(true_negatives, paired_ims)
    results['fp'] = pool.starmap(false_positives, paired_ims)
    results['fn'] = pool.starmap(false_negatives, paired_ims)
    results['acc'] = pool.starmap(accuracy_score, paired_ims)
    results['f1'] = pool.starmap(f1_score, paired_ims)
    results['matthews corr'] = pool.starmap(matthews_correlation, paired_ims)

    pool.close()
    pool.join()

    stats = pd.DataFrame(results)
    stats = stats[['tp', 'tn', 'fp', 'fn', 'acc', 'f1', 'matthews corr']]

    if normalize:
        pixels_per_im = np.prod(y_true.shape[1:])
        for col in ['tp', 'tn', 'fp', 'fn']:
            stats[col] /= pixels_per_im

    return stats.describe()


def binarize(data, threshold=None):
    """
    Forces a numpy.ndarray to have binary values (0. or 1.)

    Args:
        data: (numpy.ndarray)
        threshold: (int or float, default=None) Greater values are mapped to 1,
            lesser and equal values are mapped to 0.

    Returns: (numpy.ndarray)
        Array with binary values and the same shape as the input
    """
    if not threshold:
        threshold = np.median(data)
    return np.where(data > threshold, 1., 0.)


def true_positives(y_true, y_pred):
    """
    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (int)
        Count of true positives, guaranteed in [0, &infin)
    """
    return np.sum(np.logical_and(y_true == 1, y_pred == 1))


def true_negatives(y_true, y_pred):
    """
    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (int)
        Count of true negatives, guaranteed in [0, &infin)
    """
    return np.sum(np.logical_and(y_true == 0, y_pred == 0))


def false_positives(y_true, y_pred):
    """
    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (int)
        Count of false positives, guaranteed in [0, &infin)
    """
    return np.sum(np.logical_and(y_true == 0, y_pred == 1))


def false_negatives(y_true, y_pred):
    """
    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (int)
        Count of false negatives, guaranteed in [0, &infin)
    """
    return np.sum(np.logical_and(y_true == 1, y_pred == 0))


def binary_confusion_mat(y_true, y_pred):
    """
    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: tuple[int, int, int, int]
        True positive, true negatives, false positive, false negatives
    """
    return (
        true_positives(y_true, y_pred),
        true_negatives(y_true, y_pred),
        false_positives(y_true, y_pred),
        false_negatives(y_true, y_pred),
    )


def recall(y_true, y_pred):
    """
    Calculates the recall of a binary classifier.
    Recall is sometimes referred to as sensitivity, hit rate, or true positive
    rate.

    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (float)
        Assumes value in [0, 1]
    """
    tp, tn, fp, fn = binary_confusion_mat(y_true, y_pred)
    return 1. * tp / (tp + fn + np.finfo(float).eps)


def specificity(y_true, y_pred):
    """
    Calculates the specificity of a binary classifier.
    Specificity is sometimes referred to as the true negative rate.

    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (float)
        Assumes value in [0, 1]
    """
    tp, tn, fp, fn = binary_confusion_mat(y_true, y_pred)
    return 1. * tn / (tn + fp + np.finfo(float).eps)


def precision(y_true, y_pred):
    """
    Calculates the precision of a binary classifier.
    Precison is sometimes referred to as the positive predictive value.

    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (float)
        Assumes value in [0, 1]
    """
    tp, tn, fp, fn = binary_confusion_mat(y_true, y_pred)
    return 1. * tp / (tp + fp + np.finfo(float).eps)


def negative_predictive_value(y_true, y_pred):
    """
    Calculates the negative predictive value of a binary classifier.

    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (float)
        Assumes value in [0, 1]
    """
    tp, tn, fp, fn = binary_confusion_mat(y_true, y_pred)
    return 1. * tn / (tn + fn + np.finfo(float).eps)


def accuracy_score(y_true, y_pred):
    """
    Accuracy is sometimes referred to as the Rand index.

    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (float)
        Assumes value in [0, 1]
    """
    tp, tn, fp, fn = binary_confusion_mat(y_true, y_pred)
    return 1. * (tp + tn) / (tp + tn + fp + fn + np.finfo(float).eps)


def f1_score(y_true, y_pred):
    """
    Calculates the F1 score of a binary classifier.

    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (float)
        Assumes value in [0, 1]
    """
    p = precision(y_true, y_pred)
    r = recall(y_true, y_pred)
    return 2. * p * r / (p + r + np.finfo(float).eps)


def matthews_correlation(y_true, y_pred):
    """
    The Matthews correlation coefficient (MCC) utilizes all elements of the
    confusion matrix, unlike the F1 score, which fails to utilize true negatives.
    A MCC value of +1 represents perfect prediction, 0 represents prediction
    that is no better than random guessing, and -1 represents perfectly incorrect
    predictions.

    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (float)
        Assumes value in [-1, 1]
    """
    tp, tn, fp, fn = binary_confusion_mat(y_true, y_pred)
    return ((tp * tn) - (fp * fn)) / (np.sqrt((tp + fp) * (tp + fn) * (tn + fp) * (tn + fn)) + np.finfo(float).eps)


def informedness(y_true, y_pred):
    """
    Informedness is a component of the Matthews correlation coefficient that
    corresponds with information flow.

    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (float)
        Assumes value in [-1, 1]
    """
    return recall(y_true, y_pred) + specificity(y_true, y_pred) - 1.


def markedness(y_true, y_pred):
    """
    Markedness is a component of the Matthews correlation coefficient that
    corresponds with information flow and is the inverse of informedness.

    Args:
        y_true: (numpy.ndarray) Binary target values.
        y_pred: (numpy.ndarray) Binary predictions.

    Returns: (float)
        Assumes value in [-1, 1]
    """
    return precision(y_true, y_pred) + negative_predictive_value(y_true, y_pred) - 1.


def select_threshold(val_y, val_pred, objective=matthews_correlation, maximize=True):
    """
    Uses optimizers from scipy to select an optimal binarization threshold using
    validation data.

    Args:
        val_y: (numpy.ndarray)
        val_pred: (numpy.ndarray)
        objective: (Callable[[numpy.ndarray, numpy.ndarray], float])
            Binary statistic calculated from val_y and val_pred to be optimized.
        maximize: (bool) Perform maximization rather than minimization

    Returns: (float)
        Selected threshold, assumes value in [0, 1]
    """
    sign = -1. if maximize else 1.

    def evaluate(threshold):
        """
        Currently evaluates once on the entire volume, may want to map over slices
        and return the mean or median?
        """
        y_true = binarize(val_y, threshold=threshold)
        y_pred = binarize(val_pred, threshold=threshold)
        return sign * objective(y_true, y_pred)

    result = minimize_scalar(evaluate, bounds=(0, 1), method='bounded')
    return result.x
