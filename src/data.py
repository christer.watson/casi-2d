"""
Provides methods that prepare simulated and observational density, 12CO, and
13CO data to be used for training and testing predictive models, as well as
helper functions for loading and managing the data.
"""


import re
from pathlib import Path

import numpy as np
from astropy.io import fits


def gen_batches(
        x,
        y,
        batch_size=8,
        shuffle=True,
        x_aug=None,
        y_aug=None,
):
    """
    Creates a generator that yields batches of training data
    
    Args:
        x: numpy.ndarray, Input data.
        y: numpy.ndarray, Training labels.
        batch_size: int, Number of samples yielded per batch.
        shuffle: bool, Shuffle the data on each epoch.
        x_aug: keras.preprocessing.image.ImageDataGenerator, Augments inputs.
        y_aug: keras.preprocessing.image.ImageDataGenerator, Augments labels.
    
    Yields: Tuple[numpy.ndarray, numpy.ndarray],
        A batch of input data and corresponding training labels.
    """
    x_batch, y_batch = [], []
    while True:
        if shuffle:
            inds = np.random.permutation(np.arange(len(x)))
            x, y = x[inds], y[inds]

        count = 0
        for x_sample, y_sample in zip(x, y):
            seed = np.random.randint(2**32)
            if x_aug is not None:
                x_sample = x_aug.random_transform(x_sample, seed=seed)
            if y_aug is not None:
                y_sample = y_aug.random_transform(y_sample, seed=seed)

            x_batch.append(x_sample)
            y_batch.append(y_sample)
            count += 1

            if not (count % batch_size):
                full_x, full_y = np.stack(x_batch), np.stack(y_batch)
                x_batch, y_batch = [], []
                yield full_x, full_y
        # Provide partial batch at end of epoch
        if x_batch:
            full_x, full_y = np.stack(x_batch), np.stack(y_batch)
            x_batch, y_batch = [], []
            yield full_x, full_y


def load_co_data(folder='../data/co'):
    """
    Args:
        folder: str, Path to the data.

    Returns: Tuple[numpy.ndarray, numpy.ndarray],
        Raw data and training labels.
    """
    return (
        np.concatenate(load_fits(get_co_files(folder))),
        np.concatenate(load_fits(get_co_tracer_files(folder))),
    )


def load_density_data():
    files = [
        '../data/density/wind_2cr_flatrho_2315_256.fits',
        '../data/density/wind_2cr_flattracer_2315_256.fits',
    ]
    x, y = load_fits(files)
    return x, y


def process_classify_co_data(x, y, cutoff_frac=0.01):
    """
    Prepares the CO data for classification.

    Args:
        x: numpy.ndarray, Input data.
        y: numpy.ndarray, Target values.
        cutoff_frac: float, Value used to threshold the labels.

    Returns: Tuple[numpy.ndarray, numpy.ndarray],
        Processed data and training labels..
    """
    x = pad_data(normalize(x))
    y = pad_data(range_frac_threshold(y, cutoff_frac))
    return x[..., np.newaxis], y[..., np.newaxis]


def log_scaling_op(x):
    """
    Args:
        x: numpy.ndarray, Input data

    Returns: numpy.ndarray,
        Modified inputs, squished by a log transform.
    """
    return np.log10(1. + x - np.min(x))


def process_regress_co_data(x, y):
    """
    Collects and prepares the CO data for regression.

    Args:
        x: numpy.ndarray, Input data.
        y: numpy.ndarray, Target values.

    Returns: Tuple[numpy.ndarray, numpy.ndarray],
        Input data and corresponding training labels.
    """
    x = pad_data(normalize(log_scaling_op(x)))
    y = pad_data(normalize(log_scaling_op(y)))
    return x[..., np.newaxis], y[..., np.newaxis]


def process_classify_density_data(x, y, label_threshold=0.01):
    """
    Collects and prepares the density data for classification.

    Args:
        x: numpy.ndarray, Input data.
        y: numpy.ndarray, Target values.
        label_threshold: float, Value used to threshold the labels.

    Returns: Tuple[numpy.ndarray, numpy.ndarray],
        Input data and corresponding training labels.
    """
    y = np.where((y / x) > label_threshold, 1., 0.)
    x = normalize(x)
    return x[..., np.newaxis], y[..., np.newaxis]


def process_regress_density_data(x, y):
    """
    Collects and prepares the density data for regression.

    Args:
        x: numpy.ndarray, Input data.
        y: numpy.ndarray, Target values.

    Returns: Tuple[numpy.ndarray, numpy.ndarray],
        Input data and corresponding training labels.
    """
    y = np.clip(y / x, 0., 1.)
    x = normalize(x)
    return x[..., np.newaxis], y[..., np.newaxis]


def get_ngcc1333_data():
    """
    Collects and prepares the NGCC 1333 observational data.

    Returns: numpy.ndarray,
        Prepared observational data.
    """
    with fits.open('../data/observational/ngc1333_13co.fits') as f:
        x = f[0].data
    x = np.transpose(x)
    x = np.where(np.isnan(x), 0, x)
    x = normalize(x)
    x = pad_data(x, pads=((0, 0), (0, 175), (0, 160), (0, 0)))
    return x


def get_perseus_data():
    """
    Collects and prepares the Perseus B5 12CO data.

    Returns: numpy.ndarray,
        Prepared observational data.
    """
    with fits.open('../data/observational/perseus_b5_12co_nnorm.fits') as f:
        x = f[0].data
    x = np.where(np.isnan(x), 0, x)
    x = normalize(x)
    x = pad_data(x, pads=((0, 0), (0, 76), (0, 76)))
    return x[..., np.newaxis]


def get_co_files(data_path):
    """
    Collects all of the CO files in a directory.

    Args:
        data_path: str, Directory containing CO files

    Returns: list[str],
        Paths associated with CO files
    """
    return [
        str(x) for x in sorted(Path(data_path).glob('*.fits'))
        if not re.match(r'.*tracer.*', str(x))
    ]


def get_co_tracer_files(data_path):
    """
    Collects all of the CO tracer files in a directory.

    Args:
        data_path: str, Directory containing tracer files

    Returns: list[str],
        Paths associated with tracer files
    """
    return [
        str(x) for x in sorted(Path(data_path).glob('*.fits'))
        if re.match(r'.*tracer.*', str(x))
    ]


def load_fits(files):
    """
    Reads a list of FITS files and collects the first data block.

    Args:
        files: list[str], List of FITS files.

    Returns: list[numpy.ndarray],
        Contents of the provided FITS files as numpy.ndarrays.
    """
    output_data = []

    for file in files:
        with fits.open(file) as fits_data:
            output_data.append(fits_data[0].data)
    return output_data


def prediction_to_fits(pred, out_path='../models/prediction.fits'):
    """
    Args:
        pred: numpy.ndarray, Model predictions to be output.
        out_path: str,
    """
    fits.PrimaryHDU(np.squeeze(pred)).writeto(
        out_path,
        overwrite=True,
        output_verify='warn'
    )


def triplet_to_fits(x, y, pred, out_path='../models/triplet.fits'):
    """
    Consider using CompImageHDU to decrease disk usage.
    Test using an empty PrimaryHDU and three ImageHDU or CompImageHDUs.

    Args:
        x: numpy.ndarray, Model input data.
        y: numpy.ndarray, Model target/label data.
        pred: numpy.ndarray, Model output/prediction.
        out_path: (str)

    Returns:
    """
    hdul = fits.HDUList([
        fits.PrimaryHDU(np.squeeze(x)),
        fits.ImageHDU(np.squeeze(y)),
        fits.ImageHDU(np.squeeze(pred)),
    ])
    hdul.writeto(out_path, overwrite=True, output_verify='warn')


def pad_data(data, pads=((0, 0), (0, 1), (0, 1)), value=0.):
    """
    Args:
        data: numpy.ndarray,
        pads: tuple[tuple[int]], Specify the pre- and post-pended padding for each dimension.
        value: float or int, Value used in the padding.

    Returns: numpy.ndarray,
        Padded data.
    """
    return np.pad(
        data,
        pads,
        'constant',
        constant_values=value,
    )


def slice_density(data):
    """
    Args:
        data: numpy.ndarray, 3D simulated density data.

    Returns: numpy.ndarray,
        New array containing slices along each primary axis of the provided array.
    """
    slices = np.empty((np.sum(data.shape), data.shape[1], data.shape[2]))
    slice_count = 0

    for i in range(data.shape[0]):
        slices[slice_count, :] = data[i, :, :]
        slice_count += 1

    for j in range(data.shape[1]):
        slices[slice_count, :] = data[:, j, :]
        slice_count += 1

    for k in range(data.shape[2]):
        slices[slice_count, :] = data[:, :, k]
        slice_count += 1
    return slices


def normalize(data):
    """
    Args:
        data: numpy.ndarray

    Returns: numpy.ndarray
        Array with zero mean and unit variance.
    """
    data = data - np.mean(data)
    data = data / np.std(data)
    return data


def range_frac_threshold(data, range_frac):
    """
    Args:
        data: numpy.ndarray,
        range_frac: float,

    Returns: numpy.ndarray,
        Same shape as input array, values binarized by calculated threshold.
    """
    threshold = (1 - range_frac) * np.min(data) + range_frac * np.max(data)
    return np.where(data > threshold, 1., 0.)
